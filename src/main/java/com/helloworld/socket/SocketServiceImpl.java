package com.helloworld.socket;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class SocketServiceImpl {

    @Autowired
    private SimpMessagingTemplate msgTemplate;

    private ObjectMapper objectMapper = new ObjectMapper();

    public boolean callSocketMessage(String content) {
        msgTemplate.convertAndSend("/topic/notify", content);
        return true;
    }



}
