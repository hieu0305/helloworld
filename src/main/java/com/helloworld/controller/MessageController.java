package com.helloworld.controller;

import com.helloworld.commons.TaskPool;
import com.helloworld.socket.SocketServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private SocketServiceImpl socketService;


    @GetMapping
    ResponseEntity<?> sendMessage(){

        TaskPool.executor.execute(() -> {
            while (true){
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                socketService.callSocketMessage("Xin chào !");
            }
        });


        return new ResponseEntity<>("OKE", HttpStatus.OK);
    }


}
