package com.helloworld.commons;

import okhttp3.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

public class ThangTMSupport {

    public static final Logger logger = LogManager.getLogger(ThangTMSupport.class);

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100, TimeUnit.SECONDS)
            .writeTimeout(100, TimeUnit.SECONDS)
            .build();

    String resRawContent;

    public static void main(String[] args) {
        ThangTMSupport thangTMSupport = new ThangTMSupport();
        try {
            thangTMSupport.mainRun();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String mark = "816989877151990";

    public String random() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(200);
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < 15; i++) {
            String a = Long.parseLong(mark) + 1 + "";
            mark = a;

            if (mark.length() > 15) {
                System.out.println("Error >>> 15");
                return null;
            }
            buffer.append("50000.").append(a).append(".10004415911706.VTT\n");

        }

        System.out.println(buffer.toString());
        return buffer.toString();
    }

    public void mainRun() throws NoSuchAlgorithmException, InvalidKeyException, IOException, InterruptedException {
        while (true) {
            String data = random();

            String[] split = data.split("\n");
            EncoderUtils encoderUtils = new EncoderUtils();

            String nccCode = "5ed8b186c9e1ca526588237d";
            String gameCode = "5edaf1247b640e7d0c84a3b5";
            String accessKey = "66b8e132d2bf48e5984b300ccd601768";

            String key = "c01ad0848ae049ba84e1f5268413e287";
            for (String card : split) {

                String[] split1 = card.split("\\.");
                String dataCard = "the_test" + split1[1] + System.currentTimeMillis() + "|" + nccCode + "|" + gameCode + "|ttc_request" + split1[1] + "|" + split1[1] + "|" + split1[2] + "|" + split1[0] + "|" + split1[3] + "|1|" + accessKey;
                System.out.println(dataCard);

                String url = "http://35.247.139.175:3333/api/CardCallBack/CreateCardRequest/?data=" + EncoderUtils.genEncoderParam(dataCard, key);

                // sent url
                Request.Builder builder1 = new Request.Builder().url(url).get();
                Request request = builder1.build();


                logger.debug("request info now!!");
                TaskPool.executor.execute(() -> {
                    Response response = null;
                    try {
                        response = client.newCall(request).execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    logger.info("EpRequest status code:" + response.code());

                    if (response.code() == 200) {
                        try {
                            resRawContent = response.body().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        logger.debug("EpRequest request raw result:" + resRawContent);
                        logger.info("ussd reply:" + resRawContent);
                    }
                });


            }
        }

    }


    public String stringBase64Encode(String input) {
        String encodedString = Base64.getEncoder().encodeToString(input.getBytes());
        return encodedString;
    }

    public String genEncoderParam(String data, String secretKey) throws Exception {
        String encodeHMACSHA256 = getEncodeHMACSHA256(data, secretKey);
        String tmp = data + "|" + encodeHMACSHA256;
        String dataEncoder = stringBase64Encode(tmp);
        return dataEncoder;
    }

    public static String getEncodeHMACSHA256(String data, String secretKey) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        byte[] bytes = sha256_HMAC.doFinal(data.getBytes());
        String hash = new String(Base64.getEncoder().encode(bytes));
        return hash;
    }

}