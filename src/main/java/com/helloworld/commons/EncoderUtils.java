package com.helloworld.commons;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Created by thangtm on 6/14/18.
 */
public class EncoderUtils {
    public static String stringBase64Encode(String input) {
        String encodedString = Base64.getEncoder().encodeToString(input.getBytes());
        return encodedString;
    }

    public static String stringBase64Decode(String input) {
        String decodedString = "";
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(input);
            decodedString = new String(decodedBytes);
        } catch (Exception e) {

        }
        return decodedString;
    }

    public static String getEncodeHMACSHA256(String data, String secretKey) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        byte[] bytes = sha256_HMAC.doFinal(data.getBytes());
        String hash = new String(Base64.getEncoder().encode(bytes));
        return hash;
    }

    public static String genEncoderParam(String data, String secretKey) throws InvalidKeyException, NoSuchAlgorithmException {
        String encodeHMACSHA256 = getEncodeHMACSHA256(data, secretKey);
        String tmp = data + "|" + encodeHMACSHA256;
        String dataEncoder = stringBase64Encode(tmp);
        return dataEncoder;
    }

    public static Boolean checkSignature(String data, String secretKey) {
        try {
            String dataDecode = stringBase64Decode(data);
            String tmp = dataDecode.substring(0, dataDecode.lastIndexOf("|"));
            String encodeHMACSHA256 = getEncodeHMACSHA256(tmp, secretKey);
            String signature = dataDecode.substring(dataDecode.lastIndexOf("|") + 1, dataDecode.length());
            if (encodeHMACSHA256.equals(signature)) {
                return true;
            } else return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static String getDataFromParam(String data) {
        String dataDecode = stringBase64Decode(data);
        String result = dataDecode.substring(0, dataDecode.lastIndexOf("|"));
        return result;
    }

    public static String encryptPass(String str) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA-256").digest(str.getBytes("UTF-8"));
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & 255);
                while (hexString.length() < 2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("0");
                    sb.append(hexString);
                    hexString = sb.toString();
                }
                stringBuffer.append(hexString);
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            return str;
        }
    }
}

